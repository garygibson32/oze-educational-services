<?php get_header(); ?>
    
    <!-- The Middle Section -->
    <?php get_template_part( 'parts/page-middle' ); ?>

    <div id="content">
        <div class="pg_content__slot_1">
            <div class="pg_content__slot_1__breadcrumb_section">
                <div class="grid clear">
                    <h1><?php the_title(); ?></h1>
                    <nav class="breadcrumb_section__breadcrumb"><span></span></nav>
                </div>
            </div>
            <div class="grid clear">
                <div class="pg_content__slot_1_content">
                    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        <div class="entry">
                            <?php the_field('blog_post'); ?>
                     
                            <?php wp_link_pages(array( 'before' => 'Pages: ', 'next_or_number' => 'number' )); ?>
                             
                            <?php the_tags( ' Tags: ', ', ', '' ); ?>
                        </div>
                        <div class="meta">
                            <svg class="icon icon-calendar"><use xlink:href="#icon-calendar"></use></svg> <?php the_time( 'F jS, Y' ); ?>
                            <svg class="icon icon-pencil"><use xlink:href="#icon-pencil"></use></svg> <?php edit_post_link( 'Edit this entry','','.' ); ?>
                        </div>
                     </div>
                </div>            
            </div>
        </div>
    </div>

<?php get_footer(); ?>