<?php get_header(); ?>
    
    <!-- The Middle Section -->
    <?php get_template_part( 'parts/page-middle' ); ?>

    <div id="content">
        <div class="pg_content__slot_1">
            <div class="pg_content__slot_1__breadcrumb_section">
                <div class="grid clear">
                    <h1><?php the_title(); ?></h1>
                    <nav class="breadcrumb_section__breadcrumb"><span></span></nav>
                </div>
            </div>
            <div class="grid clear">
                <div class="pg_content__slot_1_content">
                    <?php if ( have_posts() ):?>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <h2>Not found</h2>
                    <?php endif; ?>
                </div>            
            </div>
        </div>
    </div>

<?php get_footer(); ?>