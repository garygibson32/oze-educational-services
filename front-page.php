<?php get_header(); ?>
    
    <!-- The Middle Section -->
    <?php get_template_part( 'parts/slider' ); ?>

    <div id="content">
        <div class="content__slot_1">
            <div class="grid clear content__slot_1--bg">
                <div class="content__slot_1_content">
                    <?php the_field('slot_1'); ?>
                </div>            
            </div>
        </div>
        <div class="content__slot_2">
            <div class="grid clear">
                <div class="content__slot_2--content">
                    <?php the_field('slot_2'); ?>                    
                    <?php get_template_part('parts/content-slider'); ?>
                </div>
            </div>
        </div> 
        <div class="content__slot_3">
            <div class="grid clear">
                <h2>Our Partners</h2>
                <ul class="brands">
                    <?php if ($brands = get_field( 'brands' )) : ?>
                        <?php foreach ($brands as $brand) : ?>
                            <li>
                                <?php 
                                    $brand['src'] = wp_get_attachment_image_src( $brand['brand_image'], 'full' );
                                        echo '<img atl="" src="'. $brand['src'][0] .'">';
                                ?>                    
                            </li>
                        <?php endforeach; ?>       
                    <?php endif; ?>
                </ul>
            </div>
        </div>   
    </div>

<?php get_footer(); ?>