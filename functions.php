<?php

//Enqueue scripts
function theme_scripts() {
	$ver = '1.0';
	wp_enqueue_style( 'theme-style', get_stylesheet_uri(), null, $ver );
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Dosis:400,700,800|Montserrat:400,700|Sniglet:400,800' );
	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'flexslider', get_stylesheet_directory_uri() . '/js/jquery.flexslider-min.js', 'jquery', $ver, true );
	// wp_enqueue_script( 'slicknav', get_template_directory_uri() . '/js/jquery.slicknav.min.js', array(), null, true );
	wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/scripts.js', array('jquery', 'flexslider'), $ver, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );


// Register primary navigation
if ( ! function_exists( 'theme_setup' ) ) {
	function theme_setup() {

		//navigations
		register_nav_menus( array(
			'primary' => 'Primary Menu',
			'social' => 'Social Menu',
		));

		//sidebars
		register_sidebar(array(
			'name' 	=> 'Blog Sidebar',
			'id'	=> 'blog-widget',
			'description'	=> 'Widgets for the sidebar on the blog page.',
			'before_widget'	=> '<div class="aside widget %2$s" id="%1$s">',
			'after_widget'	=> '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>'
		));
	}
}
add_action( 'after_setup_theme', 'theme_setup' );

function new_excerpt_more($more) {
	global $post;
	return '... <a class="more-link" href="' . get_permalink($post->ID) . '">read more &raquo;</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

//edit user capibilities
function add_options_to_editor () {
	$role = get_role( 'editor' );
	$role->add_cap( 'edit_theme_options' ); 
	$role->add_cap( 'gravityforms_edit_forms' );
	$role->add_cap( 'gravityforms_delete_forms' );
	$role->add_cap( 'gravityforms_create_form' );
	$role->add_cap( 'gravityforms_view_entries' );
	$role->add_cap( 'gravityforms_edit_entries' );
	$role->add_cap( 'gravityforms_delete_entries' );
	$role->add_cap( 'gravityforms_view_settings' );
	$role->add_cap( 'gravityforms_edit_settings' );
	$role->add_cap( 'gravityforms_export_entries' );
	$role->add_cap( 'gravityforms_view_entry_notes' );
	$role->add_cap( 'gravityforms_edit_entry_notes' );
}
add_action( 'admin_init', 'add_options_to_editor' );

// favicon
function add_favicon() {
  	$favicon_url = get_stylesheet_directory_uri() . '/imgs/favicon.ico';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}   
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
add_action('wp_head', 'add_favicon');

// mobile detection
function mobile_viewport() {
	echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
}
add_action('wp_head', 'mobile_viewport');

// Oze Educational Services Admin Options Page
if(function_exists('acf_add_options_page')) { 
    acf_add_options_page(array(
        'page_title'    => 'Oze Educational Services Settings',
        'menu_title'    => 'Oze Educational Services Settings',
        'menu_slug'     => 'oze_educational_services_settings',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));     
}

// for debuging
function debug($data) {
	echo '<pre>';
	var_dump($data);
	echo '</pre>';
}

// returns the top parent page id
function get_top_parent_page_id() {
	global $post;
	if (isset($post)) {
		if ($post->ancestors) {
			$post_ancestors = get_post_ancestors($post);
			return end( $post_ancestors );
		} else {
			return $post->ID;
		}
	}
}

// get video data given a vimeo or youtube url
function get_video_data($video_url) {
	// parse url
	$parse = parse_url($video_url);

	// extract video id
	if ($parse['query']) {	
		parse_str($parse['query'], $query);
		$data['id'] = $query['v'];
	} else {
		$video_array = explode('/', $video_url);
		$data['id'] = array_pop($video_array);
	}
	// extract video type
	$data['type'] = str_replace(array('.com', 'www.'), '', $parse['host']);

	return $data;
}