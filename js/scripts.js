var $ = jQuery;

$(document).ready(function(){
	$('.header-slider').flexslider({
		namespace: "flex-",
		selector: ".slides > li",
		animation: "fade",
		easing: "swing",
		slideshowSpeed: 7000,
		animationSpeed: 900,
		controlNav: true,
		directionNav: true
	});

	$('.content_slideshow__flexslider').flexslider({
	    animation: "slide",
        animationLoop: true,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 2,
        maxItems: 4
	});
});

// Slicknav
//$('nav#mobile-nav').slicknav();
