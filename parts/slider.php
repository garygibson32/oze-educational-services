<div class="middle">
    <?php   
        $slides = get_field( 'slideshow' );
    ?>

    <div class="flexslider header-slider clear">
        <ul class="slides">
            <?php if ($slides) : ?>
                <?php foreach ($slides as $slide) : ?>
                    <li class="single-item">
                        <?php 
                            $image_id = $slide["image"];
                            $image_src = wp_get_attachment_image_src( $image_id, 'full' ); 
                            $title = $slide["title"]; 
                            $subtitle = $slide["subtitle"]; 

                            // check if slide should link somewhere
                            //echo isset($url) ? '<a href="' . $url . '">' : '';
                                echo '<div class="slide-wrapper" style="background-image: url(' . $image_src[0] . ')">';
                                    echo wp_get_attachment_image( $image_id, 'header_image', null, array('class' => 'attachment-featured' ) );
                                    if (isset($title)) : 
                                        echo '<div class="slide-content-wrapper">';
                                            echo '<div class="grid">';
                                                echo '<div class="slide-content">';                                    
                                                    echo '<h2>' . $title . '</h2>';
                                                    echo '<div class="slide-more-content">';
                                                        echo '<div class="slide-subtitle">' . $subtitle . '</div>';
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';
                                    endif;
                                echo '</div>';    
                            //echo isset($url) ? '</a>' : ''; 
                        ?>                    
                    </li>
                <?php endforeach; ?>        
            <?php endif; ?>
        </ul>
    </div>
</div>