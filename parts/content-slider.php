<div class="content_slideshow">
    <?php   
        $slides = get_field( 'content_slideshow' );
    ?>

    <div class="flexslider content_slideshow__flexslider clear">
        <ul class="slides">
            <?php if ($slides) : ?>
                <?php foreach ($slides as $slide) : ?>
                    <li class="single-item">
                        <?php 
                            $image_id = $slide["image"];
                            $image_src = wp_get_attachment_image_src( $image_id, 'full' ); 

                            // check if slide should link somewhere
                            //echo isset($url) ? '<a href="' . $url . '">' : '';
                                echo '<div class="slide-wrapper" style="background-image: url(' . $image_src[0] . ')">';
                                    echo wp_get_attachment_image( $image_id, 'header_image', null, array('class' => 'attachment-featured' ) );
                                echo '</div>';    
                            //echo isset($url) ? '</a>' : ''; 
                        ?>                    
                    </li>
                <?php endforeach; ?>        
            <?php endif; ?>
        </ul>
    </div>
</div>