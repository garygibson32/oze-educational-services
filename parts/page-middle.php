<div class="middle">
	<?php
	if (!$image_id = get_field('image')) {
		$image_id = get_field('default_image', 'option');
	}
	    $image_src = wp_get_attachment_image_src( $image_id, 'full' );
	?>
	<div class="page-middle" style="background-image: url('<?php echo $image_src[0]; ?>');">
	    <div></div>
	</div>    
</div>