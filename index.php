<?php get_header(); ?>
    
    <!-- The Middle Section -->
    <?php get_template_part( 'parts/page-middle' ); ?>

    <div id="content">
        <div class="blog_content__slot_1">
            <div class="blog_content__slot_1__breadcrumb_section">
                <div class="grid clear">
                    <h1>Oze Educational Services Blog</h1>
                    <nav class="breadcrumb_section__breadcrumb"><span></span></nav>
                </div>
            </div>
            <div class="grid clear">
                <div class="blog_content__slot_1_content">
                    <nav class="blog_content__slot_1_blog-sidebar">
                        <aside>
                            <?php dynamic_sidebar( 'blog-widget' ); ?>
                        </aside>
                    </nav>                
                    <div class="blog-content">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <div class="entry">
                                    <?php the_content(); ?>
                                </div>
                                <div class="meta">
                                    <svg class="icon icon-calendar"><use xlink:href="#icon-calendar"></use></svg> <?php the_time( 'F jS, Y' ); ?>
                                    <svg class="icon icon-user"><use xlink:href="#icon-user"></use></svg> <?php the_author(); ?>
                                </div>
                                <div class="postmetadata">
                                    <?php the_tags( 'Tags: ', ', ', '<br />' ); ?>
                                    <svg class="icon icon-folder-open"><use xlink:href="#icon-folder-open"></use></svg> <?php the_category( ', ' ); ?> 
                                </div>
                            </div>
                            <?php endwhile; ?>
                                            
                            <div class="navigation">
                               <div class="next-posts"><?php next_posts_link( '&laquo; Older Entries' ); ?></div>
                               <div class="prev-posts"><?php previous_posts_link( 'Newer Entries &raquo;' ); ?></div>
                            </div>
                                            
                            <?php else : ?>
                                            
                           <h2>Not Found</h2>
                                        
                        <?php endif; ?>
                    </div>                
                </div>            
            </div>
        </div>
    </div>

<?php get_footer(); ?>