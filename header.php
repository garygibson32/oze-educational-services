<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php bloginfo('name'); ?> <?php wp_title( '|', true, 'left' ); ?></title>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header>
        <?php get_template_part( 'parts/svg' ); ?>
        <div class="top">
            <div class="grid clear">
                <address>
                    <svg class="icon icon-location"><use xlink:href="#icon-location"></use></svg><span class="mls"></span>
                    8931 Childs street, Baltimore MD, 21213
                    <svg class="icon icon-phone"><use xlink:href="#icon-phone"></use></svg><span class="mls"></span>
                    410.485.2503
                </address>
                <nav class="top__social">
                    <?php wp_nav_menu( array( 'theme_location' => 'social' ) ); ?>
                </nav>
            </div>
        </div>
        <div class="bottom">
            <div class="grid clear">
                <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                <a class="bottom--btn" target="_blank" href="#">Join the team</a>
                <nav>
                    <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                </nav>
            </div>
        </div>
    </header>