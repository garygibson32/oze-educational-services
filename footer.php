    <footer class="footer">
        <div class="footer__slot_1">
            <div class="grid clear footer__slot_1--bg">
                <?php the_field('footer_slot_one', 'option') ?>
                <?php gravity_form('Sign Up', false, false, false, '', true, 12); ?>
            </div>
        </div>
        <div class="footer__slot_2">
            <div class="grid clear">
                <div class="footer__slot_2_left">
                    <div class="footer__logo_character">Oze character</div>
                    <div class="footer__contact_info">
                        <h3>Visit Us</h3>
                        <ul>
                            <li>
                                <svg class="icon icon-location"><use xlink:href="#icon-location"></use></svg><span class="mls"></span>
                                8931 Childs street, Baltimore MD, 21213
                            </li>
                            <li>
                                <svg class="icon icon-phone"><use xlink:href="#icon-phone"></use></svg><span class="mls"></span>
                                410.485.2503
                            </li>
                            <li>
                                <svg class="icon icon-envelop"><use xlink:href="#icon-envelop"></use></svg><span class="mls"></span>
                                ozetakeoff&#64;gmail.com
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="footer__slot_2_right">
                    <div class="footer__logo"></div>
                    <p>Copyright &copy; <?php echo date('Y'); ?> Gary Gibson Designs LLC</p>
                </div>
            </div>
        </div>
    </footer>

<div id="footer-web">For <a href="http://webdesignyorkpa.com/" title="Web Design of York">Website</a> issues, contact <a href="http://webdesignyorkpa.com/" title="Web Design of York">Web Design of York</a></div>

<?php wp_footer(); ?>
</body>
</html>
